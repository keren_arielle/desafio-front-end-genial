# GenialInvestimentos

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 1.6.6.

# Rodar o APP

- Instalar nodeJs 8.0.0 ou superior

- Instalar o cli
  npm install -g @angular/cli

- Instalar dependencias
  npm install

## Development server
Executar o comando ng serve para subir o servidor.
Navegar através da url `http://localhost:4200/`.
