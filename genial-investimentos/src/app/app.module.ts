import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';


import { AppComponent } from './app.component';
import { ListaServicosComponent } from './lista-servicos/lista-servicos.component';
import { QuemSomosComponent } from './quem-somos/quem-somos.component';
import { ContatoComponent } from './contato/contato.component';
import { BannerComponent } from './banners/banners.component';


@NgModule({
  declarations: [
    AppComponent,
    ListaServicosComponent,
    QuemSomosComponent,
    ContatoComponent,
    BannerComponent
  ],
  imports: [
    BrowserModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
