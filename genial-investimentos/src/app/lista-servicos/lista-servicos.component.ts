import { servicos } from '../model/servicos.model';

import { Component, OnInit } from '@angular/core';

import { ServicoService } from '../service/servicos.service';

@Component({
  selector: 'app-lista-servicos',
  templateUrl: './lista-servicos.component.html',
  styleUrls: ['./lista-servicos.component.scss'],
  providers: [ServicoService]
})
export class ListaServicosComponent implements OnInit {

  servicos: any;

  constructor(private servicoService: ServicoService) {

  }

  ngOnInit() {
    this.inicializarServicos().subscribe(res => this.servicos = res);
  }

  inicializarServicos() {
    return this.servicoService.getServicos();
  }

}
