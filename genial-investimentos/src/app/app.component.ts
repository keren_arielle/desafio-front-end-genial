import { Component } from '@angular/core';

import { ServicoService } from './service/servicos.service';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'Genial Investimentos';

  menu = false;

  toggleMenu($event) {
    return this.menu = !this.menu;
  }
}
