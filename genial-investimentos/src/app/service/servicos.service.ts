import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/rx';
import 'rxjs/add/operator/map';

import { servicos } from '../model/servicos.model';

@Injectable()
export class ServicoService {

  constructor() {}

  getServicos(): Observable<any> {
    return Observable.of(servicos);
  }

}
